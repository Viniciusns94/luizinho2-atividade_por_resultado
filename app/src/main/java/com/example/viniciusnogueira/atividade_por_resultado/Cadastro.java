package com.example.viniciusnogueira.atividade_por_resultado;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Cadastro extends AppCompatActivity {

    public EditText editText;
    public Button bt_ok;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        editText = (EditText) findViewById(R.id.editText_Nome);
        bt_ok = (Button) findViewById(R.id.bt_enviar);

        bt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                String msg = editText.getText().toString();

                intent.putExtra("NOME", msg);
                setResult(RESULT_OK, intent);

                finish();
            }
        });
    }
}
