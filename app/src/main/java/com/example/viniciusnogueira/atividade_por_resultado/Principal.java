package com.example.viniciusnogueira.atividade_por_resultado;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Principal extends AppCompatActivity {
    //psfi tab
    public static final int CODE = 2;

    public Button bt_cadastro;
    public TextView text_salve;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        bt_cadastro = (Button) findViewById(R.id.bt_principal);
        text_salve = (TextView) findViewById(R.id.tv_saudacao);

        bt_cadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Principal.this, Cadastro.class);
                startActivityForResult(intent, CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK){
            if(requestCode == CODE){
                String nome = data.getStringExtra("NOME");
                text_salve.setText("Ola, " + nome);
            }
        }
    }
}
